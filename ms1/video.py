import ffmpeg
import requests
import datetime
import numpy as np
from flask import Flask, json
from json import JSONEncoder

app = Flask(__name__)


@app.route('/video', methods=['GET'])
def get_video():

    # RESOLUTIONS:
    #     LOW -> 720x480
    #     MEDIUM -> 1280x720
    #     HIGH -> 1920x1080

    start = datetime.datetime.now()
    out, _ = (
        ffmpeg
        .input('test.mp4')
        .filter('scale', w='1920', h='1080')
        .output('pipe:', format='rawvideo', pix_fmt='rgb24')
        .run(capture_stdout=True)
    )
    video = (
        np
        .frombuffer(out, np.uint8)
        .reshape([-1, 1080, 1920, 3])
    )
    stop = datetime.datetime.now()
    execution = stop - start
    print("Video Filter time ", execution.microseconds)
    # Serialization
    #lists = video.tolist()
    #json_str = json.dumps(lists)

    r = requests.post(url="http://ms2:5001/rec", json={'w':1920, 'h':1080})
    data = r.json()
    stop = datetime.datetime.now()
    execution = stop - start
    ex_time = execution.total_seconds() * 1000
    print("Total request time ", ex_time)
    return json.dumps(data)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
