import numpy as np
from flask import Flask, json, request

app = Flask(__name__)


@app.route('/rec', methods=['POST'])
def post_rec():
    data = request.get_json()
    w = int(data['w'])
    h = int(data['h'])
    a = np.random.rand(w,h)
    m = np.mean(a)
    return json.dumps(m)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5001)
